OBJS=maestropond.o

maestropond: ${OBJS}
	gcc -g ${OBJS} -o maestropond -lm

maestropond.o: maestropond.c
	gcc -g -W -Wall -Wno-unused-parameter -c maestropond.c -o maestropond.o

clean:
	rm -f ${OBJS} maestropond

.PHONY: clean
